# This is an example Hyprland config file.
#
# Refer to the wiki for more information.

#
# Please note not all available settings / options are set here.
# For a full list, see the wiki
#

debug {
    disable_logs = false
}

# See https://wiki.hyprland.org/Configuring/Monitors/
monitor=,highres@highrr,auto,auto,vrr,0
monitor=eDP-1,highres@highrr,auto,1.25

# See https://wiki.hyprland.org/Configuring/Keywords/ for more

# Execute your favorite apps at launch
exec-once = hypridle & hyprpaper & waybar & autostart

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# Set programs that you use
$terminal = foot
$fileManager = dolphin
$menu = rofi_apps
$browser = io.gitlab.librewolf-community
$lock = hyprlock
$logout = wlogout -b 5
$code = gtk-launch com.visualstudio.code
$passwords = org.keepassxc.KeePassXC

xwayland {
	force_zero_scaling = true
}

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = us,ru
    kb_variant =
    kb_model =
    kb_options = grp:win_space_toggle
    kb_rules =

    follow_mouse = 1

    touchpad {
        natural_scroll = false
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 2.5
    gaps_out = 5
    border_size = 2
    #col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
    col.active_border = rgba(005577ff) rgba(005577ee) 45deg
    col.inactive_border = rgba(595959aa)

    layout = dwindle

    # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
    allow_tearing = false
}


group {
    col.border_active = rgba(005577ff) rgba(005577ee) 45deg
    col.border_inactive = rgba(595959aa)

    groupbar {
      col.active = rgba(005577ff)
      col.inactive = rgba(444444ff)
      font_size = 11
    }
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 10

    blur {
        enabled = true
        size = 3
        passes = 1
        
        vibrancy = 0.1696
    }

    #shadow {
    #    enabled = true
    #    range = 4
    #    render_power = 3
    #    color = rgba(1a1a1aee)
    #}

}

animations {
    enabled = true

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 7, myBezier
    animation = windowsOut, 1, 7, default, popin 80%
    animation = border, 1, 10, default
    animation = borderangle, 1, 8, default
    animation = fade, 1, 7, default
    animation = workspaces, 1, 6, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = true # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = true # you probably want this
    force_split = 2
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    #new_is_master = true
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = false
}

misc {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    force_default_wallpaper = -1 # Set to 0 or 1 to disable the anime mascot wallpapers
    render_unfocused_fps = 30
}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#per-device-input-configs for more
#device {
#    name = epic-mouse-v1
#    sensitivity = -0.5
#}

# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
# windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
windowrulev2 = suppressevent maximize, class:.* # You'll probably like this.

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod, Return, exec, $terminal
bind = $mainMod ALT, D, exec, $terminal
bind = $mainMod, W, killactive,
bind = $mainMod SHIFT, E, exec, $logout
bind = $mainMod ALT, E, exit
bind = $mainMod, Z, exec, $fileManager
bind = $mainMod, F, togglefloating,
bind = $mainMod CTRL, F, pin,
bind = $mainMod SHIFT, F, fullscreen, 
bind = $mainMod, D, exec, $menu
bind = $mainMod, B, exec, bookmark_open

#bind = $mainMod, P, pseudo, # dwindle
#bind = $mainMod, J, togglesplit, # dwindle

# Move focus with mainMod + arrow keys
bind = $mainMod, H, movefocus, l
bind = $mainMod, L, movefocus, r
bind = $mainMod, K, movefocus, u
bind = $mainMod, J, movefocus, d

bind = $mainMod CTRL, H, changegroupactive, b
bind = $mainMod CTRL, L, changegroupactive, f

bind = $mainMod SHIFT, H, movewindow, l
bind = $mainMod SHIFT, L, movewindow, r
bind = $mainMod SHIFT, K, movewindow, u
bind = $mainMod SHIFT, J, movewindow, d

bind = $mainMod, T, togglegroup
bind = $mainMod SHIFT, T, moveoutofgroup

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
#bind = $mainMod, 0, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
#bind = $mainMod SHIFT, 0, movetoworkspace, 10

# Example special workspace (scratchpad)
bind = $mainMod, S, togglespecialworkspace, magic
bind = $mainMod SHIFT, S, movetoworkspace, special:magic

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

bind = $mainMod, GRAVE, exec, rofi -show window

bind = $mainMod CTRL, up, exec, brightnessctl s 5%+; ddcutil setvcp 10 + 5
bind = $mainMod CTRL, down, exec, brightnessctl s 5%-; ddcutil setvcp 10 - 5

bind = , XF86AudioRaiseVolume, exec, wpctl set-volume -l 1.0 @DEFAULT_SINK@ 5%+
bind = , XF86AudioLowerVolume, exec, wpctl set-volume -l 1.0 @DEFAULT_SINK@ 5%-
bind = , XF86AudioPlay, exec, playerctl -p playerctld play-pause
bind = , XF86MonBrightnessUp, exec, brightnessctl s 5%+
bind = , XF86MonBrightnessDown, exec, brightnessctl s 5%-

bind = CTRL ALT, up, exec, wpctl set-volume -l 1.0 @DEFAULT_SINK@ 5%+
bind = CTRL ALT, down, exec, wpctl set-volume -l 1.0 @DEFAULT_SINK@ 5%-
bind = $mainMod, M, exec, wpctl set-mute @DEFAULT_SOURCE@ toggle

bind = $mainMod, P, exec, playerctl -p playerctld play-pause
bind = $mainMod CTRL, R, exec, tb obs_togglerecord

bind = $mainMod CTRL, Y, exec, ytl

bind = $mainMod, X, exec, $browser
bind = $mainMod, C, exec, $code
bind = $mainMod, V, exec, $passwords

# Notifications
bind = $mainMod CTRL, N, exec, dunstctl close-all
bind = $mainMod SHIFT, N, exec, dunstctl history-pop
bind = $mainMod, N, exec, dunstctl context 

#screenshots
bind = , Print, exec, hyprshot -z -m active -m output
bind = $mainMod, Print, exec, hyprshot -z -m window
bind = $mainMod SHIFT, Print, exec, hyprshot -z -m region
bind = $mainMod CTRL, Print, exec, hyprshot -z -m region -t 3000

bind = $mainMod, 0, exec, $lock
bindl = ,switch:Lid Switch, exec, $lock

bind = $mainMod CTRL, Z, focuswindow, class:([Ss]potify)
bind = $mainMod CTRL, X, focuswindow, class:(org.telegram.desktop)

binds {
  allow_workspace_cycles = true
}
bind = SUPER, Tab, workspace, previous

#window rules
windowrulev2 = workspace 1, class:(LibreWolf)
windowrulev2 = workspace 1, class:(Org.chromium.Chromium)
windowrulev2 = workspace 8, title:^(Spotify( Free)?)$
windowrulev2 = workspace 8, class:(org.qbittorrent.qBittorrent)
windowrulev2 = workspace 8, class:(thunderbird|org.mozilla.Thunder[Bb]ird)
windowrulev2 = workspace 8, class:(org.nicotine_plus.Nicotine)
windowrulev2 = workspace 9, class:(org.telegram.desktop)
windowrulev2 = workspace 9, title:^(?!.*(Media viewer))
windowrulev2 = workspace 9, class:(com.ktechpit.whatsie)
windowrulev2 = workspace 9, class:(discord|com.discordapp.Discord)

windowrulev2 = group set, title:^(Spotify( Free)?)$
windowrulev2 = group set, class:(org.qbittorrent.qBittorrent)
windowrulev2 = group set, class:(thunderbird|org.mozilla.Thunder[Bb]ird)
windowrulev2 = group set, class:(ord.nicotine_plus.Nicotine)
windowrulev2 = group set, class:(Element|im.riot.Riot)
windowrulev2 = group set, class:(org.telegram.desktop)
windowrulev2 = group set, class:(com.ktechpit.whatsie)
windowrulev2 = group set, class:(discord|com.discordapp.Discord)

windowrulev2 = noanim, class:(org.telegram.desktop)

windowrulev2 = float, title:^About LibreWolf$
windowrulev2 = float, title:^Remmina Remote Desktop Client$

windowrulev2 = float, title:^Picture-in-Picture$
windowrulev2 = pin, title:^Picture-in-Picture$
windowrulev2 = move 1310 730, title:^Picture-in-Picture$
windowrulev2 = size 600 340, title:^Picture-in-Picture$
windowrulev2 = group barred, title:^Picture-in-Picture$

windowrulev2 = pin, title: ^Picture in picture$

windowrulev2 = float, class:(org.keepassxc.KeePassXC)
windowrulev2 = float, title:^File Transfer.*$
